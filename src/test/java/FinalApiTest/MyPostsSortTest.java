package FinalApiTest;

import com.google.common.base.Predicate;
import io.restassured.path.json.JsonPath;
import org.hamcrest.text.IsEmptyString;
import org.junit.Assert;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static com.google.common.base.Predicates.equalTo;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.isEmptyOrNullString;

public class MyPostsSortTest extends AbstractTest{
    @Test
    @DisplayName("Сортировка постов по возрастанию ASC")
    // @Tag("Positive")
    void getMyPostsASCTest(){
        JsonPath response = given()
                .header("X-Auth-Token", getToken())
                .queryParam("sort", "createdAt")
                .queryParam("order","ASC")
                .queryParam("page","1")
                .when()
                .get(getBaseUrl()+getPathGetPosts())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();
      assertThat(response.getString("data[0].title"), equalTo("Мой первый пост"));
      if (Integer.parseInt(response.getString("meta.count")) > 4) {
            assertThat(response.getString("meta.nextPage"),equalTo("1"));
        } else {
          //Assert.assertThat("", IsEmptyString.isEmptyOrNullString());
        assertThat(response.getString("meta.nextPage"), (Predicate<String>) isEmptyOrNullString());
        }
    }

    @Test
    @DisplayName("Сортиовка постов по убыванию DESC")
  //  @Tag("Positive")
    void getMyPostsDESCTest() {
        JsonPath response = given()
                .header("X-Auth-Token", getToken())
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl() + getPathGetPosts())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();
        assertThat(response.getString("data[0].title"), equalTo("Последний пост"));
        if (Integer.parseInt(response.getString("meta.count")) > 4) {
            assertThat(response.getString("meta.nextPage"), equalTo("1"));
            // } else {
           // Assert.assertThat("", IsEmptyString.isEmptyOrNullString());
         // assertThat(response.getString("meta.nextPage"), isEmptyOrNullString());
        }
    }

    private void assertThat(String string, Predicate<String> equalTo) {
    }

    @Test
    @DisplayName("Получение постов со 2-й страницы")
  //  @Tag("Positive")
    void getMyPostsPage2Test() {
        JsonPath response = given()
                .header("X-Auth-Token", getToken())
                .queryParam("sort", "createdAt")
                .queryParam("page", "2")
                .when()
                .get(getBaseUrl() + getPathGetPosts())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();
        assertThat(response.getString("meta.prevPage"), equalTo("1"));
        if (Integer.parseInt(response.getString("meta.count")) > 10) {
            assertThat(response.getString("meta.nextPage"), equalTo("3"));
        } else {
            assertThat(response.getString("meta.nextPage"),  equalTo("null"));
        }
    }

    @Test
    @DisplayName("Сортировка с невалидным значением query")
  //  @Tag("Negative")
    void getMyPostsBedRequestTest() {
        JsonPath response = given()
                .header("X-Auth-Token", getToken())
                .queryParam("order", "&&&&&")
                .when()
                .get(getBaseUrl() + getPathGetPosts())
                .then()
                .statusCode(400)
                .extract().body().jsonPath();
        assertThat(response.getString("message"), equalTo("Неверный запрос"));
    }


    @Test
    @DisplayName("Получение постов с невалидным токеном")
   // @Tag("Negative")
    void getMyPostsInvalidTokenTest(){
        JsonPath response = given()
                .header("X-Auth-Token", "???")
                .when()
                .get(getBaseUrl()+getPathGetPosts())
                .then()
                .assertThat()
                .statusCode(401)
                .extract().jsonPath();
        assertThat(response.getString("message"), equalTo("Недействительные учетные данные"));

    }
    @Test
    @DisplayName("Получение постов без токена")
   // @Tag("Negative")
    void getMyPostsWithoutTokenTest(){
        JsonPath response = given()
                .when()
                .get(getBaseUrl()+getPathGetPosts())
                .then()
                .assertThat()
                .statusCode(401)
                .extract().jsonPath();
        assertThat(response.getString("message"), equalTo("Необходимо значение токена"));
    }



}
