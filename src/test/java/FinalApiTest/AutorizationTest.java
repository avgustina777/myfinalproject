package FinalApiTest;

import com.google.common.base.Predicate;
import io.restassured.path.json.JsonPath;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

import static com.google.common.base.Predicates.equalTo;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;

public class AutorizationTest extends AbstractTest {
    @Test
    @DisplayName("Вход с валидными данными")
    @Tag("Positive")
    void AvtorizationValidTest (){


        JsonPath body = given()
                .contentType("multipart/form-data")
                .multiPart("username", getLogin())
                .multiPart("password", getPass())
                .when()
                .post(getBaseUrl()+getPathLogin())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();

        assertThat(body.getString("username"), equalTo("GB202302d8e3c11"));
        assertThat(body.getString("token"));


    }

    private <T extends @Nullable Object> void assertThat(String username, Predicate<T> gb202302d8e3c11) {
    }

    private void assertThat(String username) {
    }

    @ParameterizedTest
    @CsvFileSource(resources = "parametrsLogin.csv")
    @DisplayName("Вход с невалидными данными")
    @Tag("Negative")
    void AutorizationInvalidTest(String username, String ps){


        JsonPath body = given()
                .contentType("multipart/form-data")
                .multiPart("username", username)
                .multiPart("password", ps)
                .when()
                .post(getBaseUrl()+getPathLogin())
                .then()
                .statusCode(401)
                .extract().body().jsonPath();

        assertThat(body.getString("error"), equalTo("Неверные учетные данные"));
        assertThat(body.getString("code"), equalTo("401"));

    }


}
