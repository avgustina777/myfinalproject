package FinalApiTest;

import com.google.common.base.Predicate;
import io.restassured.path.json.JsonPath;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static com.google.common.base.Predicates.equalTo;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;

public class OtherPostSortTest extends AbstractTest {
    @Test
    @DisplayName("Получение чужих постов с сортировкой ASC")
    @Tag("Positive")
    public void getOtherPostsAsc(){
        JsonPath response =given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ASC")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl()+getPathGetPosts())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();
        assertThat(response.getString("meta.nextPage"), equalTo("2"));
        assertThat(response.getString("data[0].title"), equalTo("жареные сосиски"));
    }

    private void assertThat(String string, Predicate<String> equalTo) {
    }

    @Test
    @DisplayName("Получение чужих постов с сортировкой DESC")
    @Tag("Positive")
    public void getOtherPostsDesc(){
        JsonPath response =given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "DESC")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl()+getPathGetPosts())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();
        assertThat(response.getString("meta.nextPage"), equalTo("2"));
    }
    @Test
    @DisplayName("Получение чужих постов с сортировкой ALL")
    @Tag("Positive")
    public void getOtherPostsALL(){
        JsonPath response =given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .queryParam("sort", "createdAt")
                .queryParam("order", "ALL")
                .queryParam("page", "1")
                .when()
                .get(getBaseUrl()+getPathGetPosts())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();
        assertThat(response.getString("meta.nextPage"), equalTo("2"));
    }

    @Test
    @DisplayName("Получение чужих постов без сортировки")
    @Tag("Positive")
    public void getOtherPostsWithoutSort(){
        JsonPath response =given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMe")
                .when()
                .get(getBaseUrl()+getPathGetPosts())
                .then()
                .statusCode(200)
                .extract().body().jsonPath();
        assertThat(response.getString("meta.nextPage"), equalTo("2"));
    }
    @Test
    @DisplayName("Получение чужих постов с невалидным значением query")
    @Tag("Negative")
    public void getOtherPostsBedRequest(){
        JsonPath response =given()
                .header("X-Auth-Token", getToken())
                .queryParam("owner", "notMyyyyy")
                .when()
                .get(getBaseUrl()+getPathGetPosts())
                .then()
                .statusCode(400)
                .extract().body().jsonPath();
        assertThat(response.getString("message"), equalTo("Неверный запрос"));
    }

}
